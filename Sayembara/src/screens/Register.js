import React, {useState, createRef} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  TextInput,
  Dimensions,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import EmailTextField from '../components/EmailTextField';
import PasswordTextField from '../components/PasswordTextField';
import RoundedTextField from '../components/RoundedTextField';
import RoundedButton from '../components/RoundedButton';
// import EmailTextField from '../components/EmailTextField';
// import PasswordTextField from '../components/PasswordTextField';
// import RoundedTextField from '../components/RoundedTextField';
// import RoundedButton from '../components/RoundedButtton';

const {widht, height} = Dimensions.get('window');
const Register = ({navigation}) => {
  return (
    <SafeAreaView>
      <View>
        <Text style={styles.titleText}>Sign UP</Text>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <RoundedTextField placeHolder="Full Name" />
          <EmailTextField placeholder="EmailAddress" />
          <RoundedTextField placeHolder="Phone Number" />
          <PasswordTextField placeHolder="Password" />
          <RoundedButton
            buttonStyle={styles.loginButtonStyle}
            title="SignUP"
            textStyle={styles.signInTextColorStyle}></RoundedButton>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  titleText: {
    fontSize: 30,
    fontWeight: 'bold',
    marginHorizontal: 30,
    marginBottom: 30,
    marginTop: 20,
  },

  loginButtonStyle: {
    backgroundColor: 'black',
  },
  signInTextColorStyle: {
    color: 'white',
  },
});

export default Register;
