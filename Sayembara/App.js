import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-native-stack';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import WelcomeScreen from './src/screens/WelcomeScreen';
import Login from './src/screens/Login';
import Register from './src/screens/Register';
import Homepage from './src/screens/Homepage';
import Profile from './src/screens/Profile';

const ScreenFlow = createStackNavigator({
  Welcome: WelcomeScreen,
  Login: Login,
  Register: Register,
});

const App = createAppContainer(ScreenFlow);
export default App;
